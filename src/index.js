import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/App.jsx';
import * as serviceWorker from './serviceWorker';
import City from './pages/City.jsx';

import {
  BrowserRouter,
  Link,
  Route,
  Switch,
} from 'react-router-dom';

import createBrowserHistory from 'history/createBrowserHistory';
import Header from './components/Header';

const history = createBrowserHistory();

ReactDOM.render(
  (
    <div>
      <BrowserRouter history={history}>
        <Link to="/"><Header /></Link>
        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/cities/:id" component={City} />
          <Route component={() => <h2>Not found</h2>} />
        </Switch>
      </BrowserRouter>
    </div>
  ), document.getElementById('root')
);

serviceWorker.unregister();
