import React from 'react';
import CityTable from '../components/CityTable';
import '../sass/main.css';

const App = () => (
  <div>
    <CityTable />
  </div>

);

export default App;
