import React from 'react';
import ModalClick from './ModalClick';
import { Modal, InputGroup, FormControl, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import StorageHelpers from '../helpers/StorageHelpers';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import PropTypes from 'prop-types';


class EditSight extends ModalClick {

  constructor(props) {
    super(props);

    this.state = {
      city: this.props.city,
      descriptionField: this.props.sight.description,
      nameField: this.props.sight.name,
      sight: this.props.sight,
      sightRating: this.props.sight.rating,
    };
  }

    updateCities = () => 0;

    updateName = event => {
      // eslint-disable-next-line prefer-destructuring,no-invalid-this
      const { sight } = this.state;

      // eslint-disable-next-line no-invalid-this
      this.setState({
        nameField: event.target.value,
        sight: { ...sight, name: event.target.value },
      });
    };

    updateDescription = event => {

      // eslint-disable-next-line prefer-destructuring,no-invalid-this
      const { sight } = this.state;

      // eslint-disable-next-line no-invalid-this
      this.setState({
        descriptionField: event.target.value,
        sight: { ...sight, description: event.target.value },
      });
    };

    updateSight = () => {
      // eslint-disable-next-line no-invalid-this,prefer-destructuring
      const { city, sight } = this.state;
      const changeIndex = city.sights.findIndex(searchSight => Number(searchSight.id) === Number(sight.id));
      // eslint-disable-next-line no-invalid-this
      const cityCopy = JSON.parse(JSON.stringify(city));

      cityCopy.sights.splice(changeIndex, 1, sight);

      // eslint-disable-next-line no-undef,no-invalid-this
      this.setState({ city: cityCopy });

      const cityIndex = StorageHelpers.getCities()
      // eslint-disable-next-line no-invalid-this
        .findIndex(searchCity => Number(searchCity.id) === Number(city.id));

      // eslint-disable-next-line no-invalid-this
      StorageHelpers.updateCity(cityIndex, cityCopy);

      // eslint-disable-next-line no-invalid-this
      this.props.updateCity(cityCopy);
    };

    updateRatingField = event => {
      const [
        ,
        ,
        value,
      ] = event;

      // eslint-disable-next-line no-invalid-this
      this.setState({ sightRating: value });

      // eslint-disable-next-line no-invalid-this
      this.state.sight.rating = value;
    };

    // eslint-disable-next-line max-lines-per-function
    render() {
      // eslint-disable-next-line prefer-destructuring
      const { city, nameField, descriptionField, sightRating } = this.state;

      const viewData = {
        applyText: 'Update',
        body: () => (
          <div>
            <Modal.Body>
              <p>Change Sight data:</p>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text>Name</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl value={nameField} onChange={this.updateName} />
              </InputGroup>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text>Description</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl value={descriptionField} onChange={this.updateDescription} />
              </InputGroup>
              <InputGroup className="mb-3">
                <DropdownButton
                  onSelect={this.updateRatingField}
                  as={InputGroup.Prepend}
                  variant="outline-secondary"
                  title="Rating"
                  id="ratingDropDown"
                >
                  <Dropdown.Item href="#/5">5</Dropdown.Item>
                  <Dropdown.Item href="#/4">4</Dropdown.Item>
                  <Dropdown.Item href="#/3">3</Dropdown.Item>
                  <Dropdown.Item href="#/2">2</Dropdown.Item>
                  <Dropdown.Item href="#/1">1</Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item href="#/0">0</Dropdown.Item>
                </DropdownButton>
                <FormControl disabled value={sightRating} />
              </InputGroup>
            </Modal.Body>
          </div>
        ),
        onApply: this.updateSight,
        title: `Update sight in ${city.name}`,
      };

      return (
        <>
          {this.modalView(viewData)}
          <Button className="main-button" onClick={this.handleShow}>Edit sight</Button>
        </>
      );
    }

}

EditSight.defaultProps = {
  city: {},
  descriptionField: '',
  nameField: '',
  sight: {},
  sightRating: '',
  updateCity: null,
  updateSights: null,
};

EditSight.propTypes = {
  city: PropTypes.shape({
    bestSight: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rating: PropTypes.string,
    sights: PropTypes.arrayOf(PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.string,
      name: PropTypes.string,
      rating: PropTypes.string,
    })),
  }),

  descriptionField: PropTypes.string,
  nameField: PropTypes.string,

  sight: PropTypes.shape({
    description: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rating: PropTypes.string,
  }),
  sightRating: PropTypes.string,
  updateCity: PropTypes.func,
  updateSights: PropTypes.func,
};

export default EditSight;
