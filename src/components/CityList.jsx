import React from 'react';
import { Link } from 'react-router-dom';
import EditCity from './EditCity';
import DeleteCity from './DeleteCity';
import PropTypes from 'prop-types';


const CityList = React.memo(props => {
  const cityList = [];

  for (const [, value] of props.cities.entries()) {
    cityList.push(<tr key={`city_${value.id}`}>
      <td>
        <Link to={`/cities/${value.id}`}>
          {value.name}
        </Link>
      </td>
      <td>{value.description}</td>
      <td>{value.bestSight}</td>
      <td>{value.rating}</td>
      <td>
        <EditCity
          updateCities={props.updateCities}
          id={value.id}
        />
        <DeleteCity
          updateCities={props.updateCities}
          id={value.id}
        />
      </td>
    </tr>);
  }

  return cityList;
});

CityList.defaultProps = {
  cities: [],
  updateCities: null,
};

CityList.propTypes = {
  cities: PropTypes.arrayOf(PropTypes.shape({
    city: PropTypes.shape({
      bestSight: PropTypes.string,
      description: PropTypes.string,
      id: PropTypes.string,
      name: PropTypes.string,
      rating: PropTypes.string,
      sights: PropTypes.arrayOf(PropTypes.shape({
        description: PropTypes.string,
        id: PropTypes.string,
        name: PropTypes.string,
        rating: PropTypes.string,
      })),
    }),
  })),
  updateCities: PropTypes.func,
};

export default CityList;
