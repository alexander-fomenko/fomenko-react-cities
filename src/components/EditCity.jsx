import React from 'react';
import ModalClick from './ModalClick';
import StorageHelpers from '../helpers/StorageHelpers';
import { FormControl, InputGroup, Modal, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

class EditCity extends ModalClick {

  constructor(props) {
    super(props);

    this.state = {
      descriptionField: this.cities[this.index].description,
      nameField: this.cities[this.index].name,
    };

    this.currentCity = this.getCurrentCity();
  }

  getCurrentCity = () => {
    const cities = StorageHelpers.getCities();
    // eslint-disable-next-line no-invalid-this
    const cityIndex = cities.findIndex(city => Number(city.id) === Number(this.props.id));

    return cities[cityIndex];
  };

  updateCity = () => {
    // eslint-disable-next-line prefer-destructuring,no-invalid-this
    const { descriptionField, nameField } = this.state;
    // eslint-disable-next-line prefer-destructuring,no-invalid-this
    const { bestSight, rating, sights } = this.currentCity;
    // eslint-disable-next-line prefer-destructuring,no-invalid-this
    const { id } = this.props;

    // eslint-disable-next-line no-invalid-this
    StorageHelpers.updateCity(this.index, {
      bestSight,
      description: descriptionField,
      id,
      name: nameField,
      rating,
      sights,
    });
  };

  render() {
    // eslint-disable-next-line prefer-destructuring
    const { nameField, descriptionField } = this.state;

    const viewData = {
      applyText: 'Save',
      body: () => (
        <div>
          <Modal.Body>
            <p>Change City data:</p>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text>Name</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl value={nameField} onChange={this.updateName} />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text>Description</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl value={descriptionField} onChange={this.updateDescription} />
            </InputGroup>
          </Modal.Body>
        </div>
      ),
      onApply: this.updateCity,
      title: 'Edit city',
    };

    return (
      <>
        {this.modalView(viewData)}
        <Button className="main-button" onClick={this.handleShow}>Edit</Button>
      </>
    );
  }

}

EditCity.propTypes = { id: PropTypes.string };
EditCity.defaultProps = { id: '' };

export default EditCity;
