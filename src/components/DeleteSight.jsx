import React from 'react';
import ModalClick from './ModalClick';
import { Modal, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import StorageHelpers from '../helpers/StorageHelpers';
import PropTypes from 'prop-types';

class DeleteSight extends ModalClick {

  constructor(props) {
    super(props);

    this.state = {
      city: this.props.city,
      sight: this.props.sight,
    };
  }

    deleteSight = () => {
      // eslint-disable-next-line no-invalid-this
      const deleteIndex = this.state.city.sights.findIndex(sight => Number(sight.id) === Number(this.state.sight.id));
      // eslint-disable-next-line no-invalid-this,react/no-access-state-in-setstate
      const cityCopy = JSON.parse(JSON.stringify(this.state.city));

      cityCopy.sights.splice(deleteIndex, 1);
      // eslint-disable-next-line no-invalid-this
      this.setState({ city: cityCopy });

      const cityIndex = StorageHelpers.getCities()
      // eslint-disable-next-line no-invalid-this
        .findIndex(city => Number(city.id) === Number(cityCopy.id));

      // eslint-disable-next-line no-invalid-this
      StorageHelpers.updateCity(cityIndex, cityCopy);
      // eslint-disable-next-line no-invalid-this
      this.props.updateCity(cityCopy);
    };

    render() {
      // eslint-disable-next-line prefer-destructuring
      const { sight, city } = this.state;

      const viewData = {
        applyText: 'Delete',
        body: () => (
          <div>
            <Modal.Body>
              <p>
                Do you really want to delete
                {sight.name}
                ?
              </p>
            </Modal.Body>
          </div>
        ),
        onApply: this.deleteSight,
        title: `Delete sight from ${city.name}`,
      };

      return (
        <>
          {this.modalView(viewData)}
          <Button className="main-button" onClick={this.handleShow}>Delete sight</Button>
        </>
      );
    }

}

DeleteSight.propTypes = {
  city: PropTypes.shape({
    bestSight: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rating: PropTypes.string,
    sights: PropTypes.arrayOf(PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.string,
      name: PropTypes.string,
      rating: PropTypes.string,
    })),
  }),

  sight: PropTypes.shape({
    description: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rating: PropTypes.string,
  }),

  updateCity: PropTypes.func,
  updateSights: PropTypes.func,
};
DeleteSight.defaultProps = {
  city: {},
  sight: {},
  updateCity: null,
  updateSights: null,
};

export default DeleteSight;
