import React from 'react';

import CityList from './CityList';
import ModalClick from './ModalClick';
import SortButton from '../helpers/Utils';
import StorageHelpers from '../helpers/StorageHelpers';

class CityTable extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      cities: StorageHelpers.getCities(),
      isSortByNameActive: false,
      isSortByRatingActive: false,
    };

    this.handleUpdateCities = this.updateCities.bind(this);
    this.handleSwitchNameArrowState = this.switchNameArrowState.bind(this);
    this.handleSwitchRatingArrowState = this.switchRatingArrowState.bind(this);
  }

  updateCities = updatedCities => {
    // eslint-disable-next-line no-invalid-this
    this.setState({ cities: JSON.parse(JSON.stringify(updatedCities)) });
  };

  switchNameArrowState = () => {
    // eslint-disable-next-line no-invalid-this
    this.setState({
      isSortByNameActive: true,
      isSortByRatingActive: false,
    });
  };

  switchRatingArrowState = () => {
    // eslint-disable-next-line no-invalid-this
    this.setState({
      isSortByNameActive: false,
      isSortByRatingActive: true,
    });
  };

  render() {
    // eslint-disable-next-line prefer-destructuring
    const { cities, isSortByNameActive, isSortByRatingActive } = this.state;

    return (
      <div>
        <table className="city-table">
          <tbody>
            <tr>
              <td>
                <SortButton
                  isSortByName="true"
                  label="Name"
                  updateList={this.handleUpdateCities}
                  switchNameArrowState={this.handleSwitchNameArrowState}
                  switchRatingArrowState={this.handleSwitchRatingArrowState}
                  isSortByNameActive={isSortByNameActive}
                  isSortByRatingActive={isSortByRatingActive}
                />
              </td>
              <td>Description</td>
              <td>Best Sight</td>
              <td>
                <SortButton
                  isSortByName={false}
                  label="Rating"
                  updateList={this.handleUpdateCities}
                  switchNameArrowState={this.handleSwitchNameArrowState}
                  switchRatingArrowState={this.handleSwitchRatingArrowState}
                  isSortByNameActive={isSortByNameActive}
                  isSortByRatingActive={isSortByRatingActive}
                />
              </td>
              <td>Actions</td>
            </tr>
            <CityList cities={cities} updateCities={this.handleUpdateCities} />
          </tbody>
        </table>
        <ModalClick updateCities={this.handleUpdateCities} />
      </div>
    );
  }

}

export default CityTable;
