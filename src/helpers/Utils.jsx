import React from 'react';
import PropTypes from 'prop-types';
import Glyphs from '../components/Glyphs';
import { INDEX_ERROR, INDEX_SUCCESS } from './Constants';
import { Button } from 'react-bootstrap';
import StorageHelpers from './StorageHelpers';

class SortButton extends React.PureComponent {

  static proceedSortStatic = (object1, object2) => {
    const obj1 = object1.name.toLowerCase();
    const obj2 = object2.name.toLowerCase();

    if (obj1 < obj2) return INDEX_ERROR;
    if (obj1 > obj2) return INDEX_SUCCESS;

    return 0;
  };

  static sortByNameStatic = sortingObjects => {
    // eslint-disable-next-line no-invalid-this
    sortingObjects.sort((object1, object2) => this.proceedSortStatic(object1, object2));
  };

  constructor(props) {
    super(props);

    this.state = {
      sortOrderByName: false,
      sortOrderByRating: false,
    };
  }

  getCityById = id => {
    const cities = StorageHelpers.getCities();
    const cityIndex = cities.findIndex(city => Number(city.id) === Number(id));

    return cities[cityIndex];
  };

  updateSights = sights => {
    // eslint-disable-next-line no-invalid-this,prefer-destructuring
    const { id, updateList } = this.props;

    const cities = StorageHelpers.getCities();
    const cityIndex = cities.findIndex(city => Number(city.id) === Number(id));
    const updatedCity = JSON.parse(JSON.stringify(cities[cityIndex]));

    updatedCity.sights = sights;

    updateList(updatedCity);
  };

  updateCities = cities => {
    // eslint-disable-next-line prefer-destructuring,no-invalid-this
    const { updateList } = this.props;

    updateList(cities);
  };

  sortByName = () => {
    // eslint-disable-next-line prefer-destructuring,no-invalid-this
    const { sortOrderByName } = this.state;
    // eslint-disable-next-line no-invalid-this,prefer-destructuring
    const { id, switchNameArrowState } = this.props;

    // eslint-disable-next-line no-invalid-this
    const sortingObject = this.getSortObject(id);

    // eslint-disable-next-line no-invalid-this
    sortingObject.sort((object1, object2) => this.proceedSort(
      object1,
      object2,
      sortOrderByName,
      'name',
    ));

    // eslint-disable-next-line no-invalid-this
    this.setState({ sortOrderByName: !sortOrderByName }, switchNameArrowState());

    // eslint-disable-next-line no-invalid-this
    this.updateSortingList(sortingObject);
  };

  getSortObject = id => (typeof id === 'number'
    // eslint-disable-next-line no-invalid-this
    ? this.getCityById(id).sights
    : StorageHelpers.getCities());

  sortByRating = () => {
    // eslint-disable-next-line prefer-destructuring,no-invalid-this
    const { sortOrderByRating } = this.state;
    // eslint-disable-next-line no-invalid-this,prefer-destructuring
    const { id, switchRatingArrowState } = this.props;

    // eslint-disable-next-line no-invalid-this
    const sortingObject = this.getSortObject(id);

    // eslint-disable-next-line no-invalid-this
    sortingObject.sort((object1, object2) => this.proceedSort(
      object1,
      object2,
      sortOrderByRating,
      'rating',
    ));

    // eslint-disable-next-line no-invalid-this
    this.setState({ sortOrderByRating: !sortOrderByRating }, switchRatingArrowState());

    // eslint-disable-next-line no-invalid-this
    this.updateSortingList(sortingObject);
  };

  proceedSort = (object1, object2, sortOrder, property) => {
    const obj1 = object1[property].toString().toLowerCase();
    const obj2 = object2[property].toString().toLowerCase();

    if (sortOrder) {
      if (obj1 > obj2) return INDEX_ERROR;
      if (obj1 < obj2) return INDEX_SUCCESS;
    } else {
      if (obj1 < obj2) return INDEX_ERROR;
      if (obj1 > obj2) return INDEX_SUCCESS;
    }

    return 0;
  };

  updateSortingList = sortingObject => {
    try {
      if (typeof sortingObject[0].sights === 'undefined') {
        // eslint-disable-next-line no-invalid-this
        this.updateSights(sortingObject);
      } else {
        // eslint-disable-next-line no-invalid-this
        this.updateCities(sortingObject);
      }
      // eslint-disable-next-line no-empty
    } catch {}
  };

  render() {
    // eslint-disable-next-line prefer-destructuring
    const { sortOrderByName, sortOrderByRating } = this.state;
    // eslint-disable-next-line prefer-destructuring
    const { isSortByNameActive, isSortByRatingActive } = this.props;
    // eslint-disable-next-line prefer-destructuring
    const { label, isSortByName } = this.props;

    const sortByNameButton = () => (
      <Button className="main-button" onClick={this.sortByName}>
        {label}
        &nbsp;
        {isSortByNameActive && <Glyphs direction={sortOrderByName} />}
      </Button>
    );

    const sortByRatingButton = () => (
      <Button className="main-button" onClick={this.sortByRating}>
        {label}
        &nbsp;
        {isSortByRatingActive && <Glyphs direction={sortOrderByRating} />}
      </Button>
    );

    return (
      isSortByName ? sortByNameButton() : sortByRatingButton()
    );
  }

}

SortButton.defaultProps = {
  id: '',
  isSortByName: false,
  isSortByNameActive: false,
  isSortByRatingActive: false,
  label: '',
  switchNameArrowState: null,
  switchRatingArrowState: null,
  updateList: null,
};

SortButton.propTypes = {
  id: PropTypes.string,
  isSortByName: PropTypes.bool,
  isSortByNameActive: PropTypes.bool,
  isSortByRatingActive: PropTypes.bool,
  label: PropTypes.string,
  switchNameArrowState: PropTypes.func,
  switchRatingArrowState: PropTypes.func,
  updateList: PropTypes.func,
};

export default SortButton;
