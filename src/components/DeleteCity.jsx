import React from 'react';
import ModalClick from './ModalClick';
import StorageHelpers from '../helpers/StorageHelpers';
import { Modal, Button } from 'react-bootstrap';

class DeleteCity extends ModalClick {

  constructor(props) {
    super(props);

    this.state = { nameField: this.cities[this.index].name };
  }

  deleteCity = () => {
    // eslint-disable-next-line no-invalid-this
    StorageHelpers.deleteCity(this.index);
  };

  render() {
    // eslint-disable-next-line prefer-destructuring
    const { nameField } = this.state;
    const viewData = {
      applyText: 'Delete',
      body: () => (
        <div>
          <Modal.Body>
            <p>
              Do you really want to delete&nbsp;
              {nameField}
              ?
            </p>
          </Modal.Body>
        </div>
      ),
      onApply: this.deleteCity,
      title: 'Delete city',
    };

    return (
      <>
        {this.modalView(viewData)}
        <Button className="main-button" onClick={this.handleShow}>Delete</Button>
      </>
    );
  }

}

export default DeleteCity;
