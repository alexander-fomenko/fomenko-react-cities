import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown } from '@fortawesome/free-solid-svg-icons';
import '../sass/main.css';
import PropTypes from 'prop-types';

const Glyphs = React.memo(props => {
  // eslint-disable-next-line react/prop-types
  const { direction } = props;

  return <FontAwesomeIcon className={direction ? 'arrow_active' : 'arrow'} icon={faArrowDown} />;
});

Glyphs.defaultProps = { direction: true };
Glyphs.propsType = { direction: PropTypes.bool };


export default Glyphs;
