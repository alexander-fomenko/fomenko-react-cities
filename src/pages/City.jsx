import React from 'react';
import PropTypes from 'prop-types';

import AddSight from '../components/AddSight';
import DeleteSight from '../components/DeleteSight';
import EditSight from '../components/EditSight';
import SightTable from '../components/SightTable';
import SortButton from '../helpers/Utils';
import StorageHelpers from '../helpers/StorageHelpers';

class City extends React.PureComponent {

  constructor(props) {
    super(props);

    const city = this.getCurrentCity();

    city.rating = this.calculateCityRating(city.sights);
    city.bestSight = this.getBestSight(city.sights);

    this.state = {
      city,
      isSortByNameActive: false,
      isSortByRatingActive: false,
    };

    this.handleUpdateCity = this.updateCity.bind(this);
    this.handleSwitchNameArrowState = this.switchNameArrowState.bind(this);
    this.handleSwitchRatingArrowState = this.switchRatingArrowState.bind(this);
  }

  switchNameArrowState = () => {
    // eslint-disable-next-line no-invalid-this
    this.setState({
      isSortByNameActive: true,
      isSortByRatingActive: false,
    });
  };

  switchRatingArrowState = () => {
    // eslint-disable-next-line no-invalid-this
    this.setState({
      isSortByNameActive: false,
      isSortByRatingActive: true,
    });
  };

  getCurrentCity = () => {
    const cities = StorageHelpers.getCities();
    // eslint-disable-next-line prefer-destructuring,react/destructuring-assignment,no-invalid-this
    const { id } = this.props.match.params;

    // eslint-disable-next-line no-invalid-this
    const index = cities.findIndex(city => city.id === Number(id));

    return cities[index];
  };

  updateCity = updatedCity => {
    const city = updatedCity;

    // eslint-disable-next-line no-invalid-this
    city.rating = this.calculateCityRating(city.sights);
    // eslint-disable-next-line no-invalid-this
    city.bestSight = this.getBestSight(city.sights);
    // eslint-disable-next-line no-invalid-this
    this.setState({ city });

    const cityIndex = StorageHelpers.getCities()
      .findIndex(searchCity => Number(searchCity.id) === Number(updatedCity.id));

    StorageHelpers.updateCity(cityIndex, city);
  };

  getBestSight = sights => {
    if (sights.length === 0) return 'No sights';

    const highestRate = Math.max(...sights.map(sight => Number(sight.rating)));

    const bestSightIndex = sights.findIndex(sight => Number(sight.rating) === highestRate);

    return sights[bestSightIndex].name;
  };

  calculateCityRating = sights => {
    if (sights.length === 0) return 0;

    const rateArray = sights.map(sight => Number(sight.rating));

    return rateArray.reduce((prev, current) => prev + current) / rateArray.length;
  };

  // eslint-disable-next-line max-lines-per-function
  render() {
    // eslint-disable-next-line prefer-destructuring
    const { city, isSortByNameActive, isSortByRatingActive } = this.state;

    return (
      <>
        <table className="city-table">
          <tbody>
            <tr>
              <td colSpan="4">
                {city.name}
                :&nbsp;
                {city.description}
              </td>
            </tr>
            <tr>
              <td colSpan="3">
                Best Sight:
                &nbsp;
                {city.bestSight}
              </td>
              <td>
                Rating:
                {city.rating}
              </td>
            </tr>
            <tr>
              <td>
                <SortButton
                  isSortByName="true"
                  label="Sight name"
                  id={city.id}
                  updateList={this.handleUpdateCity}
                  switchNameArrowState={this.handleSwitchNameArrowState}
                  switchRatingArrowState={this.handleSwitchRatingArrowState}
                  isSortByNameActive={isSortByNameActive}
                  isSortByRatingActive={isSortByRatingActive}
                />
              </td>
              <td>Description</td>
              <td>
                <SortButton
                  isSortByName={false}
                  label="Rating"
                  id={city.id}
                  updateList={this.handleUpdateCity}
                  switchNameArrowState={this.handleSwitchNameArrowState}
                  switchRatingArrowState={this.handleSwitchRatingArrowState}
                  isSortByNameActive={isSortByNameActive}
                  isSortByRatingActive={isSortByRatingActive}
                />
              </td>

              <td>Actions</td>
            </tr>
            <SightTable
              city={city}
              sights={city.sights}
              updateCity={this.handleUpdateCity}
            >
              <EditSight />
              <DeleteSight />
            </SightTable>
          </tbody>
        </table>
        <AddSight city={city} updateCity={this.handleUpdateCity} />
      </>
    );
  }

}
City.defaultProps = { match: {} };
City.propTypes = { match: PropTypes.shape({ params: PropTypes.shape({ id: PropTypes.string }) }) };

export default City;
