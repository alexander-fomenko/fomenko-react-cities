import React from 'react';
import PropTypes from 'prop-types';

class SightTable extends React.PureComponent {

  constructor(props) {
    super(props);

    // eslint-disable-next-line prefer-destructuring
    const { city } = this.props;

    this.state = { city };
  }

  setSightProperty = sight => {
    // eslint-disable-next-line no-invalid-this,prefer-destructuring
    const { city } = this.state;
    // eslint-disable-next-line no-invalid-this,prefer-destructuring
    const { children, updateCity } = this.props;

    return (
      // eslint-disable-next-line no-invalid-this
      React.Children.map(children, child => React.cloneElement(child, {
        city,
        sight,
        updateCity,
      }))
    );
  };

  render() {
    const sightList = [];
    // eslint-disable-next-line prefer-destructuring
    const { sights } = this.props;
    // eslint-disable-next-line prefer-destructuring

    for (const [, sight] of sights.entries()) {
      sightList.push(<tr>
        <td>{sight.name}</td>
        <td>{sight.description}</td>
        <td>{sight.rating}</td>
        <td>{this.setSightProperty(sight)}</td>
      </tr>);
    }

    return (
      sightList
    );
  }
}

SightTable.defaultProps = {
  children: [],
  city: '',
  sights: '',
  updateCity: null,
};

SightTable.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape({
    city: PropTypes.shape({
      bestSight: PropTypes.string,
      description: PropTypes.string,
      id: PropTypes.number,
      name: PropTypes.string,
      rating: PropTypes.number,
      sights: PropTypes.arrayOf(PropTypes.shape({
        description: PropTypes.string,
        id: PropTypes.number,
        name: PropTypes.string,
        rating: PropTypes.number,
      })),
    }),

    sights: PropTypes.arrayOf(PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.number,
      name: PropTypes.string,
      rating: PropTypes.number,
    })),

    updateCity: PropTypes.func,
  })),

  city: PropTypes.shape({
    bestSight: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.number,
    name: PropTypes.string,
    rating: PropTypes.number,
    sights: PropTypes.arrayOf(PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.number,
      name: PropTypes.string,
      rating: PropTypes.number,
    })),
  }),

  sights: PropTypes.arrayOf(PropTypes.shape({
    description: PropTypes.string,
    id: PropTypes.number,
    name: PropTypes.string,
    rating: PropTypes.number,
  })),

  updateCity: PropTypes.func,
};

export default SightTable;
