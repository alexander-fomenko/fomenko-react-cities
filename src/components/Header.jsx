import React from 'react';

const Header = React.memo(() => (
  <nav className="header">
    <p>Home</p>
  </nav>
));

export default Header;
