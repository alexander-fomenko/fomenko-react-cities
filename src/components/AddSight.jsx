import React from 'react';
import ModalClick from './ModalClick';
import { Modal, InputGroup, FormControl, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import PropTypes from 'prop-types';
import SortButton from '../helpers/Utils';

class AddSight extends ModalClick {

  constructor(props) {
    super(props);

    this.state = {
      city: this.props.city,
      sightRating: 0,
    };
  }

  updateCity = () => {
    // eslint-disable-next-line no-invalid-this,prefer-destructuring
    const { city, descriptionField, nameField, sightRating } = this.state;

    const id = city.sights.length ? Math.max(...city.sights.map(sight => parseInt(sight.id))) + 1 : 1;

    const cityCopy = JSON.parse(JSON.stringify(city));

    cityCopy.sights.push({
      description: descriptionField,
      id,
      name: nameField,
      rating: sightRating,
    });

    SortButton.sortByNameStatic(cityCopy.sights);

    // eslint-disable-next-line no-invalid-this
    this.setState({
      city: cityCopy,
      descriptionField: '',
      nameField: '',
      sightRating: 0,
    });
    // eslint-disable-next-line no-invalid-this
    this.props.updateCity(cityCopy);
  };

  updateRatingField = event => {
    const [
      ,
      ,
      value,
    ] = event;

    // eslint-disable-next-line no-invalid-this
    this.setState({ sightRating: value });
  };

  // eslint-disable-next-line max-lines-per-function
  render() {
    // eslint-disable-next-line prefer-destructuring
    const { nameField, descriptionField, sightRating } = this.state;

    const viewData = {
      applyText: 'Add Sight',
      body: () => (
        <div>
          <Modal.Body>
            <p>Put the data of a new Sight here:</p>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text>Name</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl value={nameField} onChange={this.updateName} />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text>Description</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl value={descriptionField} onChange={this.updateDescription} />
            </InputGroup>
            <InputGroup className="mb-3">
              <DropdownButton
                onSelect={this.updateRatingField}
                as={InputGroup.Prepend}
                variant="outline-secondary"
                title="Rating"
                id="ratingDropDown"
              >
                <Dropdown.Item href="#/5">5</Dropdown.Item>
                <Dropdown.Item href="#/4">4</Dropdown.Item>
                <Dropdown.Item href="#/3">3</Dropdown.Item>
                <Dropdown.Item href="#/2">2</Dropdown.Item>
                <Dropdown.Item href="#/1">1</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#/0">0</Dropdown.Item>
              </DropdownButton>
              <FormControl disabled value={sightRating} />
            </InputGroup>
          </Modal.Body>
        </div>
      ),

      onApply: this.updateCity,
      title: 'Add new Sight',
    };

    return (
      <div>
        {this.modalView(viewData)}
        <Button className="main-button" onClick={this.handleShow}>Add sight</Button>
      </div>
    );
  }

}

AddSight.defaultProps = {
  city: {},
  updateCity: null,
};

AddSight.propTypes = {
  city: PropTypes.shape({
    bestSight: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rating: PropTypes.string,
    sights: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
  }),
  updateCity: PropTypes.func,
};

export default AddSight;
