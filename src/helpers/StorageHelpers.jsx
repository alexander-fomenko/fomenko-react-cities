import SortButton from './Utils';

class StorageHelpers {

    static addCity = (name, description) => {
      // eslint-disable-next-line no-invalid-this
      const cities = this.getCities();

      const id = cities.length ? Math.max(...cities.map(city => parseInt(city.id))) + 1 : 1;

      cities.push({ bestSight: '', description, id, name, rating: 0, sights: [] });

      SortButton.sortByNameStatic(cities);

      // eslint-disable-next-line no-invalid-this
      this.updateCities(cities);
    };

    static updateCities = cities => {
      localStorage.city = JSON.stringify(cities);
    };

    static updateCity = (index, city) => {
      // eslint-disable-next-line no-invalid-this
      const cities = this.getCities();

      cities[index] = city;

      // eslint-disable-next-line no-invalid-this
      this.updateCities(cities);
    };

    static deleteCity = id => {
      // eslint-disable-next-line no-invalid-this
      const cities = this.getCities();

      cities.splice(id, 1);

      // eslint-disable-next-line no-invalid-this
      this.updateCities(cities);
    };

    static getCities = () => {
      const { city } = localStorage;

      try {
        return JSON.parse(city);
      } catch (error) {
        return [];
      }
    };

}

export default StorageHelpers;
