import React from 'react';
import { Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import StorageHelpers from '../helpers/StorageHelpers';
import PropTypes from 'prop-types';

class ModalClick extends React.PureComponent {

  constructor(props) {
    super(props);

    // eslint-disable-next-line prefer-destructuring
    const { id } = this.props;

    this.cities = StorageHelpers.getCities();
    this.index = this.cities.findIndex(item => item.id === id);

    this.state = {
      descriptionField: '',
      hasBeenClicked: false,
      nameField: '',
    };
  }

  updateCities = () => {
    // eslint-disable-next-line react/destructuring-assignment,no-invalid-this,prefer-destructuring
    const { updateCities } = this.props;

    // eslint-disable-next-line no-invalid-this
    this.setState({
      descriptionField: '',
      nameField: '',
    });

    if (typeof updateCities !== 'undefined') {
      updateCities(StorageHelpers.getCities());
    }
  };

  handleShow = () => {
    // eslint-disable-next-line no-invalid-this
    this.setState({ hasBeenClicked: true });
  };

  handleClose = () => {
    // eslint-disable-next-line no-invalid-this
    this.setState({ hasBeenClicked: false });
  };

    updateName = event => {
      // eslint-disable-next-line no-invalid-this
      this.setState({ nameField: event.target.value });
    };

    updateDescription = event => {
      // eslint-disable-next-line no-invalid-this
      this.setState({ descriptionField: event.target.value });
    };

    modalView = viewData => (
      // eslint-disable-next-line no-invalid-this,react/destructuring-assignment
      <Modal show={this.state.hasBeenClicked} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{viewData.title}</Modal.Title>
        </Modal.Header>
        {viewData.body()}
        <Modal.Footer>
          {/* eslint-disable-next-line no-invalid-this */}
          <Button variant="secondary" onClick={this.handleClose}>Close</Button>
          <Button
            variant="primary"
            onClick={() => {
              viewData.onApply();
              // eslint-disable-next-line no-invalid-this
              this.updateCities();
              // eslint-disable-next-line no-invalid-this
              this.handleClose();
            }}
          >
            {viewData.applyText}
          </Button>
        </Modal.Footer>
      </Modal>
    );

    render() {
      // eslint-disable-next-line prefer-destructuring
      const { nameField, descriptionField } = this.state;
      const viewData = {
        applyText: 'Add City',
        body: () => (
          <div>
            <Modal.Body>
              <p>Put the data of a new City here:</p>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text>Name</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl value={nameField} onChange={this.updateName} />
              </InputGroup>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text>Description</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl value={descriptionField} onChange={this.updateDescription} />
              </InputGroup>
            </Modal.Body>
          </div>
        ),
        onApply: () => {
          StorageHelpers.addCity(nameField, descriptionField);
        },
        title: 'Add new City',
      };

      return (
        <div>
          {this.modalView(viewData)}
          <Button className="main-button" onClick={this.handleShow}>Add city</Button>
        </div>
      );
    }

}

ModalClick.defaultProps = {
  id: '',
  updateCities: null,
};

ModalClick.propTypes = {
  id: PropTypes.string,
  updateCities: PropTypes.func,
};

export default ModalClick;
