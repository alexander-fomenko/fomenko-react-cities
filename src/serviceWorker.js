const isLocalhost = Boolean(window.location.hostname === 'localhost'

    // [::1] is the IPv6 localhost address.
    || window.location.hostname === '[::1]'

    // 127.0.0.1/8 is considered localhost for IPv4.
    // eslint-disable-next-line require-unicode-regexp
    || window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));

// eslint-disable-next-line func-style,require-jsdoc
function registerValidSW(swUrl, config) {
  navigator.serviceWorker
    .register(swUrl)
    .then(registration => {
      registration.onupdatefound = () => {
        // eslint-disable-next-line prefer-destructuring
        const installingWorker = registration.installing;

        if (installingWorker == null) return;

        installingWorker.onstatechange = () => {
          if (installingWorker.state === 'installed') {
            if (navigator.serviceWorker.controller) {
              // eslint-disable-next-line no-console
              console.log('New content is available and will be used when all '
                + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.');

              // Execute callback
              if (config && config.onUpdate) {
                config.onUpdate(registration);
              }
            } else {
              // eslint-disable-next-line no-console
              console.log('Content is cached for offline use.');

              // Execute callback
              if (config && config.onSuccess) {
                config.onSuccess(registration);
              }
            }
          }
        };
      };
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error('Error during service worker registration:', error);
    });
}

// eslint-disable-next-line func-style,require-jsdoc
function checkValidServiceWorker(swUrl, config) {
  fetch(swUrl)
    .then(response => {
      const contentType = response.headers.get('content-type');

      if (
        // eslint-disable-next-line no-magic-numbers
        response.status === 404
        // eslint-disable-next-line no-magic-numbers
        || (contentType != null && contentType.indexOf('javascript') === -1)
      ) {
        navigator.serviceWorker.ready.then(registration => {
          registration.unregister().then(() => {
            window.location.reload();
          });
        });
      } else {
        registerValidSW(swUrl, config);
      }
    })
    .catch(() => {
      // eslint-disable-next-line no-console
      console.log('No internet connection found. App is running in offline mode.');
    });
}

// eslint-disable-next-line func-style,require-jsdoc
export function register(config) {
  // eslint-disable-next-line no-undef
  if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    // eslint-disable-next-line no-undef
    const publicUrl = new URL(process.env.PUBLIC_URL, window.location.href);

    if (publicUrl.origin !== window.location.origin) return;

    window.addEventListener('load', () => {
      // eslint-disable-next-line no-undef
      const swUrl = `${process.env.PUBLIC_URL}/service-worker.js`;

      if (isLocalhost) {
        checkValidServiceWorker(swUrl, config);

        navigator.serviceWorker.ready.then(() => {
          console.log('This web app is being served cache-first by a service '
              + 'worker. To learn more, visit https://bit.ly/CRA-PWA');
        });
      } else {
        registerValidSW(swUrl, config);
      }
    });
  }
}


// eslint-disable-next-line func-style,require-jsdoc
export function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    });
  }
}
